

package br.com.senac.ex6;

import static junit.framework.Assert.assertEquals;
import org.junit.Test;


public class TestCalculadoradeDesconto {
   
    @Test
      public void testDesconto() {
       
        Valor vl = new Valor(100, 5, 1000);
        double rest = CalculadoradeDesconto.getPreco(vl);
        assertEquals(490, rest, 0.1);
    }
    
    
}
