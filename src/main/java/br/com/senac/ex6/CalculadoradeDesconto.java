/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex6;

/**
 *
 * @author Administrador
 */
public class CalculadoradeDesconto {
    
    public static double getPreco(Valor v) {

        double precoBase = v.getQuantidade() * v.getPrecoIten();
        if (precoBase > v.getLimite()) {
            v.setFatorDesconto(0.95);

            return precoBase * v.getFatorDesconto();
        } else {
            v.setFatorDesconto(0.98);
            return precoBase * v.getFatorDesconto();
        }

    }
    
}
