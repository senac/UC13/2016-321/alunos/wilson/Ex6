/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex6;

/**
 *
 * @author Administrador
 */
public class Valor {

    private double precoIten;
    private double quantidade;
    private double fatorDesconto;
    private double limite;

    public Valor() {
    }

    public Valor(double precoIten, double quantidade, double limite) {
        this.precoIten = precoIten;
        this.quantidade = quantidade;
        this.limite = limite;
    }

    public double getPrecoIten() {
        return precoIten;
    }

    public void setPrecoIten(double precoIten) {
        this.precoIten = precoIten;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getFatorDesconto() {
        return fatorDesconto;
    }

    public void setFatorDesconto(double fatorDesconto) {
        this.fatorDesconto = fatorDesconto;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }
    
    
    

}
